﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FichaRPGteste1.Models.Enums
{
    public enum Racas
    {
        [Display(Name = "Anão")]
        Anao,
        [Display(Name = "Elfo")]
        Elfo,
        [Display(Name = "Halfling")]
        Halfling,
        [Display(Name = "Humano")]
        Humano,
        [Display(Name = "Draconato")]
        Draconato,
        [Display(Name = "Gnomo")]
        Gnomo,
        [Display(Name = "Meio-Elfo")]
        MeioElfo,
        [Display(Name = "Meio-Orc")]
        MeioOrc,
        [Display(Name = "Tiefling")]
        Tiefling
    }
}
