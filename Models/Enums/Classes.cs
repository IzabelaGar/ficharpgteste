﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FichaRPGteste1.Models.Enums
{
    public enum Classes
    {
        [Display(Name = "Barbaro")]
        Barbaro,
        [Display(Name = "Bardo")]
        Bardo,
        [Display(Name = "Bruxo")]
        Bruxo,
        [Display(Name = "Clerigo")]
        Clerigo,
        [Display(Name = "Druida")]
        Druida,
        [Display(Name = "Feiticeiro")]
        Feiticeiro,
        [Display(Name = "Guerreiro")]
        Guerreiro,
        [Display(Name = "Ladino")]
        Ladino,
        [Display(Name = "Mago")]
        Mago,
        [Display(Name = "Monge")]
        Monge,
        [Display(Name = "Paladino")]
        Paladino,
        [Display(Name = "Patrulheiro")]
        Patrulheiro

    }
}
