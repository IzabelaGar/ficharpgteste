﻿using FichaRPGteste1.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FichaRPGteste1.Models
{
    public class Personagem
    {
        public String NomeJogador { get; set; }
        public String NomePersonagem { get; set; }
        public Classes Classe { get; set; }
        [Display(Name = "Raças")]
        public Racas Raca { get; set; }


    }
}
